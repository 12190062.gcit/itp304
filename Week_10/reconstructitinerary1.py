import collections

# functions returns new itinerary of type list
def findItinary(tickets):
	# adjacency list 
	adj = collections.defaultdict(list)

	# add neighbours for each node
	for src, dest in tickets:
		adj[src].append(dest)
	
	# sort dict values to get route in smallest lexical order
	for l in adj.values():
		l.sort()

	result = ["JFK"] # starting airport
	# recursive dfs to reconstruct itinary
	def dfs(src):
		# visited all airports
		if len(result) == len(tickets)+1:
			return True
		
		# no route, backtrack
		if src not in adj:
			return False
		
		# visit adjacent nodes depth wise
		for i, v in enumerate(adj[src]):
			adj[src].pop(i) # remove item at index i from adjList
			result.append(v)
			
			if dfs(v):
				return True
			
			# backtrack
			adj[src].insert(i, v) # add v back to adjList
			result.pop() # remove last item v 

		return False

	dfs("JFK")
	return result

if __name__ == '__main__':
	# tickets = [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR", "SFO"]]
	tickets = [["JFK", "SFO"], ["JFK", "ATL"], ["SFO", "ATL"], ["ATL", "JFK"], ["ATL", "SFO"]]
	print(findItinary(tickets))
	
	
